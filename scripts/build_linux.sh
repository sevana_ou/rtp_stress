#!/bin/bash

rm -rf build
mkdir build
cd build

cmake "../../source"
cmake --build .
cp rtp_stress ..
cd ..

rm -rf build