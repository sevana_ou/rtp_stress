# Utility to replay RTP streams

Utility is capable to read single RTP stream from .pcap file and play it to network with multiplication.
So you can get .pcap with single RTP stream inside and play e.g. 5000 RTP copies simultaneously.

It is intended for stress tests. We had to make own utility - as in some cases it is much easier as sipp setup.

One can read more about our tools and technologies at https://sevana.biz.

# How to use it ?

Please use CMakeLists.txt file. Sorry, no binary builds for now.
Only Linux / macOS are supported. However Windows port should be easy.

# Usage.

./rtp_sender --input <.pcap file here> --copies 1000 --copies-delay 20 --interval 20 --threads 2 --target <IP:port>

Sends 2000 stream in 2 threads.
New streams are sent with 20ms offset between them.
Packet interval is 20ms.

# Roadmap.

Windows port.

# License

BSD license - please see source files.


Please send us your questions & comments to development@sevana.biz

Thank you!