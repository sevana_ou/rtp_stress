/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <string.h>

#include "network_frame.h"

#define ETHERTYPE_MPLS_UC   (0x8847)
#define ETHERTYPE_MPLS_MC   (0x8848)
#define ETHERTYPE_IPV6      (0x86dd)
#define ETHERTYPE_IP        (0x0800)

#define MPLS_STACK_MASK (0x00000100)
#define MPLS_STACK_SHIFT (8)


network_frame::packet_data network_frame::get_udp_payload_for_ethernet(packet_data &packet, ip_address &source, ip_address &destination)
{
    packet_data result(packet);

    const ethernet_header* ethernet = reinterpret_cast<const ethernet_header*>(packet.data);

    // Skip ethernet header
    packet.data += sizeof(ethernet_header);
    packet.length -= sizeof(ethernet_header);

    // See if there is Vlan header
    uint16_t proto = 0;
    if (ethernet->mEtherType == 129)
    {
        const vlan_header* vlan = reinterpret_cast<const vlan_header*>(packet.data);
        packet.data += sizeof(vlan_header);
        packet.length -= sizeof(vlan_header);
        proto = ntohs(vlan->mData);
    }

    // Skip MPLS headers
    switch (proto)
    {
    case ETHERTYPE_MPLS_UC:
    case ETHERTYPE_MPLS_MC:
        // Parse MPLS here until marker "bottom of mpls stack"
        for(bool bottomOfStack = false; !bottomOfStack;
            bottomOfStack = ((ntohl(*(uint32_t*)(packet.data - 4)) & MPLS_STACK_MASK) >> MPLS_STACK_SHIFT) != 0)
        {
            packet.data += 4;
            packet.length -=4;
        }
        break;

    case ETHERTYPE_IP:
        //  Next IPv4 packet
        break;

    case ETHERTYPE_IPV6:
        // Next IPv6 packet
        break;
    }

    const ip4_header* ip4 = reinterpret_cast<const ip4_header*>(packet.data);

    if (ip4->mProtocol != IPPROTO_UDP && ip4->mProtocol != 0)
        return packet_data();


    switch (ip4->version())
    {
    case 4:
        return get_udp_payload_for_ip4(packet, source, destination);

    case 6:
        return get_udp_payload_for_ip6(packet, source, destination);

    default:
        return packet_data();
    }
}

network_frame::packet_data network_frame::get_udp_payload_for_sll(packet_data &packet, ip_address &source, ip_address &destination)
{
    packet_data result(packet);

    if (packet.length < 16)
        return packet_data();

    const linux_sll_header* sll = reinterpret_cast<const linux_sll_header*>(packet.data);

    packet.data += sizeof(linux_sll_header);
    packet.length -= sizeof(linux_sll_header);

    switch (ntohs(sll->mProtocolType))
    {
    case 0x0800:
        return get_udp_payload_for_ip4(packet, source, destination);

    case 0x86DD:
        return get_udp_payload_for_ip6(packet, source, destination);

    default:
        return packet_data();
    }
}


network_frame::packet_data network_frame::get_udp_payload_for_loopback(packet_data& packet, ip_address& source, ip_address& destination)
{
    packet_data result(packet);

    if (packet.length < 16)
        return packet_data();

    struct loopback_header
    {
        uint32_t protocol_type;
    };

    const loopback_header* lh = reinterpret_cast<const loopback_header*>(packet.data);

    packet.data += sizeof(loopback_header);
    packet.length -= sizeof(loopback_header);

    switch (lh->protocol_type)
    {
    case AF_INET:
        return get_udp_payload_for_ip4(packet, source, destination);

    case AF_INET6:
        return get_udp_payload_for_ip6(packet, source, destination);

    default:
        return packet_data();
    }
}
/*
network_frame::packet_data network_frame::make_sll_packet_from_udp(packet_data& payload, timeval& tv, ip_address& source, ip_address& destination)
{
    // Build SLL header
    linux_sll_header sll_header;
    sll_header.mPacketType = 4;             // Sent by us
    sll_header.mProtocolType = 0x8000;      // IP protocols
    sll_header.mAddress = 0;
    sll_header.mAddressLength = 6;
    sll_header.mARPHRD = 0;

    ip4_header iph;
    iph.ihl = 5;
    iph.version = 4;
    iph.mTos = 16;
    iph.mId = htons(10201);
    iph.mTtl = 64;
    iph.mProtocol = 17;
    iph->saddr = inet_addr(inet_ntoa((((struct sockaddr_in *)&(ifreq_ip.ifr_addr))->sin_addr)));
    iph->daddr = inet_addr(destination_ip); // put destination IP address
}
*/


network_frame::packet_data network_frame::get_udp_payload_for_ip4(packet_data &packet, ip_address &source, ip_address &destination)
{
    packet_data result(packet);
    const ip4_header* ip4 = reinterpret_cast<const ip4_header*>(packet.data);
    if (ip4->mProtocol != IPPROTO_UDP && ip4->mProtocol != 0)
        return packet_data(nullptr, 0);

    result.data += ip4->headerLength();
    result.length -= ip4->headerLength();

    const udp_header* udp = reinterpret_cast<const udp_header*>(result.data);
    result.data += sizeof(udp_header);
    result.length -= sizeof(udp_header);

    // Check if UDP payload length is smaller than full packet length. It can be VLAN trailer data - we need to skip it
    size_t length = ntohs(udp->mDatagramLength);
    if (length - sizeof(udp_header) < (size_t)result.length)
        result.length = length - sizeof(udp_header);

    source.set_ip(ip4->mSource);
    source.set_port(ntohs(udp->mSourcePort));

    destination.set_ip(ip4->mDestination);
    destination.set_port(ntohs(udp->mDestinationPort));

    return result;
}

struct ip6_header
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
    uint8_t     traffic_class_hi:4,
        version:4;
    uint8_t     flow_label_hi:4,
        traffic_class_lo:4;
    uint16_t	flow_label_lo;

#elif __BYTE_ORDER == __BIG_ENDIAN
    uint8_t			version:4,
        traffic_class_hi:4;
    uint8_t			traffic_class_lo:4,
        flow_label_hi:4;
    uint16_t	    flow_label_lo;
#else
# error "Please fix endianness defines"
#endif

    uint16_t		payload_len;
    uint8_t			next_header;
    uint8_t			hop_limit;

    struct	in6_addr	src_ip;
    struct	in6_addr	dst_ip;
};

network_frame::packet_data network_frame::get_udp_payload_for_ip6(packet_data &packet, ip_address &source, ip_address &destination)
{
    packet_data result(packet);
    const ip6_header* ip6 = reinterpret_cast<const ip6_header*>(packet.data);
    /*if (ip6->mProtocol != IPPROTO_UDP && ip4->mProtocol != 0)
    return PacketData(nullptr, 0);
  */
    result.data += sizeof(ip6_header);
    result.length -= sizeof(ip6_header);
    //std::cout << sizeof(Ip6Header) << std::endl;

    const udp_header* udp = reinterpret_cast<const udp_header*>(result.data);
    result.data += sizeof(udp_header);
    result.length -= sizeof(udp_header);

    source.set_ip(ip6->src_ip);
    source.set_port(ntohs(udp->mSourcePort));
    //std::cout << source.toStdString() << " - ";

    destination.set_ip(ip6->dst_ip);
    destination.set_port(ntohs(udp->mDestinationPort));
    //std::cout << destination.toStdString() << std::endl;

    return result;
}
