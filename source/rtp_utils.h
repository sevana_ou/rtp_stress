/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __RTP_UTILS_H
#define __RTP_UTILS_H

#include <stdlib.h>
#include <stdint.h>

class rtp_helper
{
public:
    static bool is_rtp(const void* buffer, size_t length);
    static bool is_rtp_or_rtcp(const void* buffer, size_t length);
    static bool is_rtcp(const void* buffer, size_t length);

    static int  find_payload_length(const void* buffer, size_t length);
    static int  find_ptype(const void* buffer, size_t length);

    static uint32_t find_timestamp(const void* buffer, size_t length);
    static void set_timestamp(void* buffer, size_t length, uint32_t timestamp);

    static int  find_packet_no(const void* buffer, size_t length);
    static void set_packet_no(void* buffer, size_t length, uint16_t seqno);

    static unsigned find_ssrc(const void* buffer, size_t length);
    static void set_ssrc(void* buffer, size_t length, unsigned ssrc);
};

#endif
