/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "pcap_parser.h"
#include <string>
#include <map>
#include <ostream>
#include <mutex>
#include <memory.h>

#if defined(TARGET_LINUX)
# include <arpa/inet.h>
#endif

#define INITIAL_BUFFER_CAPACITY (1024)

class pcap_parser: public dump_parser
{
public:
    pcap_parser();
    ~pcap_parser() override;

    void parse(const void* buffer, size_t len) override;

    static bool is_my_format(uint32_t signature);

private:
    int state = 0;
    size_t packet_length = 0;
    int datalink = 0;
    pcap_frame_header packet_header;
};

class pcapng_parser: public dump_parser
{
public:
    pcapng_parser();
    ~pcapng_parser() override;

    void parse(const void* buffer, size_t len) override;

    static bool is_my_format(uint32_t signature);

private:
    enum
    {
        Block_Header,
        Block_Data,
        Block_Footer
    };
    int block_parse_state = Block_Header;
    struct block_header
    {
        uint32_t type;
        uint32_t size;
    };
    block_header block_header;

    size_t packet_length = 0;
    int datalink = 0;
    pcap_frame_header packet_header;

    enum endianess
    {
        Endianess_LittleEndian,
        Endianess_BigEndian
    };
    endianess current_endianess = Endianess_LittleEndian;
    uint16_t current_linktype = 0;
    uint8_t current_timestamp_resolution = 6;

    void parse_block();
    void parse_shb();
    void parse_idb();
    void parse_isb();
    void parse_epb();
    void parse_spb();
    void parse_nrb();
    void parse_cb();
    void skip_block();

    uint32_t read_uint32(const void* mem);
    uint16_t read_uint16(const void* mem);
    static endianess my_endianess();

};


class dump_parser::impl
{
public:
    impl();
    ~impl();

    // Stream buffer
    uint8_t* buffer = nullptr;
    size_t buffer_size = 0, buffer_capacity = 0;
    callback handler;

    // Methods for stream buffer operations - add to the end & remove from the beginning
    void append_buffer(const void* data, size_t len);
    void erase_buffer(size_t len);
};

dump_parser::impl::impl()
{
    this->buffer = reinterpret_cast<uint8_t*>(malloc(INITIAL_BUFFER_CAPACITY));
    this->buffer_capacity = INITIAL_BUFFER_CAPACITY;
    this->buffer_size = 0;
}

dump_parser::impl::~impl()
{
    if (this->buffer)
        free(this->buffer);
}

dump_parser* dump_parser::make(uint32_t signature)
{
    if (pcap_parser::is_my_format(signature))
        return new pcap_parser();

    if (pcapng_parser::is_my_format(signature))
        return new pcapng_parser();

    return nullptr;
}

void dump_parser::set_callback(callback callback)
{
    this->impl->handler = callback;
}

dump_parser::callback dump_parser::get_callback() const
{
    return this->impl->handler;
}

void dump_parser::impl::append_buffer(const void* data, size_t len)
{
    while (this->buffer_size + len > this->buffer_capacity)
    {
        this->buffer = reinterpret_cast<uint8_t*>(realloc(this->buffer, this->buffer_capacity * 2));
        if (this->buffer)
            this->buffer_capacity *= 2;
        else
            return;
    }

    memcpy(reinterpret_cast<uint8_t*>(this->buffer) + this->buffer_size, data, len);
    this->buffer_size += len;
}

void dump_parser::impl::erase_buffer(size_t len)
{
    if (len >= this->buffer_size)
        this->buffer_size = 0;
    else
    {
        memmove(this->buffer, this->buffer + len, this->buffer_size - len);
        buffer_size -= len;
    }
}

dump_parser::dump_parser()
    :impl(nullptr)
{
    this->impl = new class impl();
}

dump_parser::~dump_parser()
{
    delete this->impl; this->impl = nullptr;
}


// ------------- PcapDataParser --------------
pcap_parser::pcap_parser()
{
}

pcap_parser::~pcap_parser()
{
}

bool pcap_parser::is_my_format(uint32_t signature)
{
    bool normal_resolution = signature == 0xa1b2c3d4 || signature == 0xd4c3b2a1;
    bool ns_resolution = signature == 0xa1b23c4d || signature == 0x4d3cb2a1;

    return normal_resolution || ns_resolution;
}

struct pcap_file_header {
    uint32_t magic;
    uint16_t version_major;
    uint16_t version_minor;
    int32_t thiszone; /* gmt to local correction */
    uint32_t sigfigs;        /* accuracy of timestamps */
    uint32_t snaplen;        /* max length saved portion of each pkt */
    uint32_t linktype;       /* data link type (LINKTYPE_*) */
};

struct pcap_packet_header
{
    uint32_t ts_1;  /* time stamp */
    uint32_t ts_2;
    uint32_t caplen;        /* length of portion present */
    uint32_t len;   /* length this packet (off wire) */
};

void pcap_parser::parse(const void* buffer, size_t len)
{
    enum
    {
        State_GlobalHeader = 0,
        State_PacketHeader,
        State_PacketData
    };


    this->impl->append_buffer(buffer, len);

    for (bool enough_data = true; enough_data == true;)
    {
        switch (this->state)
        {
        case State_GlobalHeader:
            enough_data = this->impl->buffer_size >= sizeof(pcap_file_header);
            if (enough_data)
            {
                pcap_file_header* file_header = reinterpret_cast<pcap_file_header*>(this->impl->buffer);

                // Check if magic ID is ok
                bool normal_resolution = file_header->magic == 0xa1b2c3d4 || file_header->magic == 0xd4c3b2a1;
                bool ns_resolution = file_header->magic == 0xa1b23c4d || file_header->magic == 0x4d3cb2a1;

                if (!normal_resolution && !ns_resolution)
                    break;

                this->datalink = static_cast<int>(file_header->linktype);
                this->impl->erase_buffer(sizeof(pcap_file_header));
                this->state = State_PacketHeader;
            }
            break;

        case State_PacketHeader:
            enough_data = this->impl->buffer_size >= sizeof(pcap_packet_header);
            if (enough_data)
            {
                pcap_packet_header* packet_header = reinterpret_cast<pcap_packet_header*>(this->impl->buffer);
                this->packet_header.ts.tv_sec = static_cast<long>(packet_header->ts_1);
                this->packet_header.ts.tv_usec = static_cast<long>(packet_header->ts_2);
                this->packet_header.len = packet_header->len;
                this->packet_header.caplen = packet_header->caplen;
                this->impl->erase_buffer(sizeof(pcap_packet_header));
                this->state = State_PacketData;
            }
            break;

        case State_PacketData:
            enough_data = this->impl->buffer_size >= this->packet_header.caplen;
            if (enough_data)
            {
                try
                {
                    this->impl->handler(&this->packet_header, this->impl->buffer, this->datalink);
                }
                catch(...)
                {}
                this->impl->erase_buffer(this->packet_header.caplen);
                this->state = State_PacketHeader;
            }
            break;
        }
    }
}

// ----------- PcapNgDataParser ------------
pcapng_parser::pcapng_parser()
{}

pcapng_parser::~pcapng_parser()
{}

bool pcapng_parser::is_my_format(uint32_t signature)
{
    return signature == 0x0A0D0D0A;
}

void pcapng_parser::parse(const void* buffer, size_t len)
{
    this->impl->append_buffer(buffer, len);

    bool success = false;
    do
    {
        success = false;
        if  (Block_Header == this->block_parse_state)
        {
            if (this->impl->buffer_size >= 8)
            {
                this->block_header = *reinterpret_cast<struct block_header*>(this->impl->buffer);
                this->impl->erase_buffer(sizeof(block_header));
                this->block_parse_state = Block_Data;
                success = true;
            }
        }
        else
        if (Block_Data == this->block_parse_state)
        {
            if (this->impl->buffer_size >= this->block_header.size - 8)
            {
                parse_block();
                this->impl->erase_buffer(this->block_header.size - 8);
                this->block_parse_state = Block_Header;
                success = true;
            }
        }
    }
    while (success);
}

const uint32_t Type_SHB = 0x0A0D0D0A;
const uint32_t Type_IDB = 0x00000001;
const uint32_t Type_EPB = 0x00000006;
const uint32_t Type_SPB = 0x00000003;

void pcapng_parser::parse_block()
{
    switch (this->block_header.type)
    {
    case Type_SHB:  parse_shb(); break;
    case Type_IDB:  parse_idb(); break;
    case Type_EPB:  parse_epb(); break;
    case Type_SPB:  parse_spb(); break;
    default:
        skip_block();
    }
}

struct option
{
    uint16_t mId;
    std::string mValue;

    int to_int() const;
};

const uint16_t opt_endofopt  = 0;
const uint16_t opt_comment = 1;
const uint16_t opt_custom_1 = 2988;
const uint16_t opt_custom_2 = 2989;
const uint16_t opt_custom_3 = 19372;
const uint16_t opt_custom_4 = 19373;

#include <map>
typedef std::map<uint16_t, option> options_map;

static uint16_t padd_32(uint16_t len)
{
    uint16_t tail = len % 4;
    return len + (tail ? 4 - tail : 0);
}

static options_map parse_options(const uint8_t* buffer, size_t maxlen)
{
    options_map r;

    struct option_header
    {
        uint16_t mId;
        uint16_t mSize;
    };

    uint16_t offset = 0;
    const option_header* hdr = nullptr;
    for (hdr = reinterpret_cast<const option_header*>(buffer + offset);
         hdr->mId != opt_endofopt && hdr->mSize && offset < maxlen;
         hdr = reinterpret_cast<const option_header*>(buffer + offset))
    {
        option opt;
        opt.mId = hdr->mId;
        opt.mValue = std::string(reinterpret_cast<const char*>(buffer + offset + sizeof(option_header)), hdr->mSize);
        r[opt.mId] = opt;

        offset += sizeof(option_header) + padd_32(hdr->mSize);
    }

    return r;
}

// available options
static const uint16_t shb_hardware = 2;
static const uint16_t shb_os = 3;
static const uint16_t shb_userappl = 4;

void pcapng_parser::parse_shb()
{
    struct shb_header
    {
        uint32_t mByteOrderMagic;
        uint16_t mMajorVersion;
        uint16_t mMinorVersion;
        int64_t  mSectionLength;
    };

    shb_header* hdr = reinterpret_cast<shb_header*>(this->impl->buffer);
    if (hdr->mByteOrderMagic == 0x1A2B3C4D)
        this->current_endianess = Endianess_LittleEndian;
    else
        this->current_endianess = Endianess_BigEndian;

    auto shb_options = parse_options(this->impl->buffer + sizeof(shb_header), this->block_header.size - sizeof(shb_header) - 12);
    // TODO: expose some SHB options
}

void pcapng_parser::skip_block()
{
    // Do nothing
}

void pcapng_parser::parse_idb()
{
    struct idb_header
    {
        uint16_t linktype;
        uint16_t reserved;
        uint32_t snaplen;
    };

    const idb_header* hdr = reinterpret_cast<const idb_header*>(this->impl->buffer);
    this->current_linktype = hdr->linktype;
    auto idb_options = parse_options(this->impl->buffer + sizeof(idb_header), this->block_header.size - sizeof(idb_header) - 12);

    // Find current timestamp resolution
    if (idb_options.count(9))
        this->current_timestamp_resolution = static_cast<uint8_t>(idb_options[9].mValue[0]);
}

void pcapng_parser::parse_isb()
{
    skip_block();
}

static uint64_t get_timestamp(uint64_t raw, uint8_t resolution)
{
    if (resolution & 128)
    {
        resolution &= 127;
        uint32_t two_p = static_cast<uint32_t>(0x1) << resolution;
        return static_cast<uint64_t>(static_cast<double>(raw) * (1.0 / two_p) * 100000.0);
    }
    else
    {
        if (resolution > 6)
        {
            uint32_t divider = 1;
            for (int i = 0; i < resolution - 6; i++)
                divider *= 10;
            raw /= divider;
        }
        else
        if (resolution < 6)
        {
            uint32_t multiplier = 1;
            for (int i = 0; i < 6 - resolution; i++)
                multiplier *= 10;
            raw *= multiplier;
        }
    }
    return raw;
}

void pcapng_parser::parse_epb()
{
    struct epb_header
    {
        uint32_t interface_id;
        uint32_t timestamp_high;
        uint32_t timestamp_low;
        uint32_t caplen;
        uint32_t origlen;
    };

    const epb_header* hdr = reinterpret_cast<const epb_header*>(this->impl->buffer);
    uint64_t timestamp = static_cast<uint64_t>(hdr->timestamp_low) + (static_cast<uint64_t>(hdr->timestamp_high) << 32);
    timestamp = get_timestamp(timestamp, this->current_timestamp_resolution);

    if (this->impl->handler)
    {
        pcap_frame_header frame_hdr;
        frame_hdr.ts.tv_sec = timestamp / 1000000;
        frame_hdr.ts.tv_usec = timestamp % 1000000;
        frame_hdr.len = hdr->origlen;
        frame_hdr.caplen = hdr->caplen;
        this->impl->handler(&frame_hdr, this->impl->buffer + sizeof(epb_header), this->current_linktype);
    }
}

void pcapng_parser::parse_spb()
{
    struct spb_header
    {
        uint32_t origlen;
    };

    const spb_header* hdr = reinterpret_cast<const spb_header*>(this->impl->buffer);
    if (this->impl->handler)
    {
        pcap_frame_header frame_hdr;
        frame_hdr.caplen = this->block_header.size - 12;
        if (frame_hdr.caplen > hdr->origlen)
            frame_hdr.caplen = hdr->origlen;
        frame_hdr.len = hdr->origlen;
        frame_hdr.ts.tv_sec = 0;
        frame_hdr.ts.tv_usec = 0;
        this->impl->handler(&frame_hdr, this->impl->buffer + sizeof(spb_header), this->current_linktype);
    }
}

void pcapng_parser::parse_nrb()
{
    skip_block();
}
void pcapng_parser::parse_cb()
{
    skip_block();
}

pcapng_parser::endianess pcapng_parser::my_endianess()
{
    if (htonl(0x01020304) == 0x01020304)
        return Endianess_BigEndian;
    else
        return Endianess_LittleEndian;
}

static uint32_t change_byte_order_32(uint32_t v)
{
    uint8_t v0 = v & 0xFF,
            v1 = (v >> 8) & 0xFF,
            v2 = (v >> 16) & 0xFF,
            v3 = (v >> 24) & 0xFF;

    return v3 + (static_cast<uint32_t>(v2) << 8) + (static_cast<uint32_t>(v1) << 16) + (static_cast<uint32_t>(v0) << 24);
}

static uint16_t change_byte_order_16(uint16_t v)
{
    uint8_t v0 = v & 0xFF,
            v1 = (v >> 8) & 0xFF;

    return static_cast<uint16_t>(v1)
            + (static_cast<uint16_t>(v0) << 8);
}

uint32_t pcapng_parser::read_uint32(const void* mem)
{
    uint32_t raw = *reinterpret_cast<const uint32_t*>(mem);

    if (my_endianess() != this->current_endianess)
        return change_byte_order_32(raw);
    else
        return raw;
}

uint16_t pcapng_parser::read_uint16(const void* mem)
{
    uint16_t raw = *reinterpret_cast<const uint16_t*>(mem);
    if (my_endianess() != this->current_endianess)
        return change_byte_order_16(raw);
    else
        return raw;
}

// --------------- dump_writer --------------------
class dump_builder::impl
{
public:
    impl(std::ostream& output)
        :file(output)
    {
    }

    ~impl()
    {}

    void set_datalink(int n)
    {
        this->datalink = n;
    }

    int get_datalink() const
    {
        return this->datalink;
    }

    void write(pcap_frame_header& header, const void* data)
    {
        if (!header_written)
        {
            pcap_file_header fh;
            fh.linktype = this->datalink;
            fh.magic = 0xa1b2c3d4;
            fh.version_major = 2;
            fh.version_minor = 4;
            fh.thiszone = 0;
            fh.sigfigs = 0;
            fh.snaplen = 65535;
            file.write(reinterpret_cast<const char*>(&fh), sizeof(fh));
            header_written = true;
        }

        // Prepare packet header
        struct
        {
            uint32_t ts_sec;         /* timestamp seconds */
            uint32_t ts_usec;        /* timestamp microseconds */
            uint32_t incl_len;       /* number of octets of packet saved in file */
            uint32_t orig_len;       /* actual length of packet */
        } packet_hdr;

        packet_hdr.ts_sec = header.ts.tv_sec;
        packet_hdr.ts_usec = header.ts.tv_usec;
        packet_hdr.incl_len = header.caplen;
        packet_hdr.orig_len = header.len;
        file.write(reinterpret_cast<const char*>(&packet_hdr), sizeof(packet_hdr));
        file.write(reinterpret_cast<const char*>(data), packet_hdr.incl_len);
    }

private:
    std::ostream& file;
    int datalink;
    bool header_written = false;
};


dump_builder::dump_builder(std::ostream& output)
    :impl(nullptr)
{
    this->impl = new class impl(output);
}

dump_builder::~dump_builder()
{
    delete this->impl; this->impl = nullptr;
}

void dump_builder::set_datalink(int datalink)
{
    this->impl->set_datalink(datalink);
}

int dump_builder::get_datalink() const
{
    return this->impl->get_datalink();
}

void dump_builder::write(pcap_frame_header& header, const void* data)
{
    this->impl->write(header, data);
}
