/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#if defined(TARGET_WIN)
# include <WinSock2.h>
# include <Windows.h>
#endif

#include "rtp_utils.h"

#if !defined(TARGET_WIN)
# include <alloca.h>
#endif

#if defined(TARGET_LINUX)
# include <arpa/inet.h>
#endif

#include <sstream>
#include <tuple>

struct rtp_header
{
    unsigned char cc:4;         /* CSRC count             */
    unsigned char x:1;          /* header extension flag  */
    unsigned char p:1;          /* padding flag           */
    unsigned char version:2;	/* protocol version       */
    unsigned char pt:7;         /* payload type           */
    unsigned char m:1;          /* marker bit             */
    unsigned short seq;         /* sequence number        */
    unsigned int ts;            /* timestamp              */
    unsigned int ssrc;          /* synchronization source */
};

struct rtcp_header
{
    unsigned char rc:5;         /* reception report count */
    unsigned char p:1;          /* padding flag           */
    unsigned char version:2;	/* protocol version       */
    unsigned char pt:8;         /* payload type           */
    uint16_t len;               /* length                 */
    uint32_t ssrc;              /* synchronization source */
};

bool rtp_helper::is_rtp(const void* buffer, size_t length)
{
    if (length < 12 || !buffer)
        return false;

    unsigned char _type = reinterpret_cast<const rtp_header*>(buffer)->pt;
    bool rtp = ( (_type & 0x7F) >= 96 && (_type & 0x7F) < 127) || ((_type & 0x7F) < 35);
    return rtp;
}


bool rtp_helper::is_rtp_or_rtcp(const void* buffer, size_t length)
{
    if (length < 12 || !buffer)
        return false;

    unsigned char b = ((const unsigned char*)buffer)[0];

    return (b & 0xC0 ) == 128;
}

bool rtp_helper::is_rtcp(const void* buffer, size_t length)
{
    return (is_rtp_or_rtcp(buffer, length) && !is_rtp(buffer, length));
}

unsigned rtp_helper::find_ssrc(const void* buffer, size_t length)
{
    if (!buffer || length < 12)
        return 0;

    if (is_rtp(buffer, length))
        return ntohl(reinterpret_cast<const rtp_header*>(buffer)->ssrc);
    else
        return ntohl(reinterpret_cast<const rtcp_header*>(buffer)->ssrc);
}

void rtp_helper::set_ssrc(void* buffer, size_t length, unsigned ssrc)
{
    if (!buffer || length < 12)
        return;

    if (is_rtp(buffer, length))
        reinterpret_cast<rtp_header*>(buffer)->ssrc = htonl(ssrc);
    else
        if (is_rtcp(buffer, length))
            reinterpret_cast<rtcp_header*>(buffer)->ssrc = htonl(ssrc);
}


int rtp_helper::find_ptype(const void* buffer, size_t length)
{
    if (is_rtp(buffer, length))
        return reinterpret_cast<const rtp_header*>(buffer)->pt;
    else
        return -1;
}

uint32_t rtp_helper::find_timestamp(const void* buffer, size_t length)
{
    if (is_rtp(buffer, length))
        return ntohl(reinterpret_cast<const rtp_header*>(buffer)->ts);
    else
        return 0;
}

void rtp_helper::set_timestamp(void* buffer, size_t length, uint32_t timestamp)
{
    if (is_rtp(buffer, length))
        reinterpret_cast<rtp_header*>(buffer)->ts = htonl(timestamp);
}


int rtp_helper::find_packet_no(const void *buffer, size_t length)
{
    if (is_rtp(buffer, length))
        return ntohs(reinterpret_cast<const rtp_header*>(buffer)->seq);
    else
        return -1;
}

void rtp_helper::set_packet_no(void *buffer, size_t length, uint16_t seqno)
{
    if (is_rtp(buffer, length))
        reinterpret_cast<rtp_header*>(buffer)->seq = htons(seqno);
}

int rtp_helper::find_payload_length(const void* buffer, size_t length)
{
    if (is_rtp(buffer, length))
    {
        return length - 12;
    }
    else
        return -1;
}
