/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _HL_NETWORK_FRAME_H
#define _HL_NETWORK_FRAME_H

#include <stdint.h>
#include "ip_address.h"

class network_frame
{
public:
    struct packet_data
    {
        const uint8_t*  data;
        size_t          length;

        packet_data(const uint8_t* data, size_t length)
            :data(data), length(length)
        {}

        packet_data()
            :data(nullptr), length(0)
        {}
    };

    static packet_data get_udp_payload_for_ethernet(packet_data& packet, ip_address& source, ip_address& destination);
    static packet_data get_udp_payload_for_ip4(packet_data& packet, ip_address& source, ip_address& destination);
    static packet_data get_udp_payload_for_ip6(packet_data& packet, ip_address& source, ip_address& destination);
    static packet_data get_udp_payload_for_sll(packet_data& packet, ip_address& source, ip_address& destination);
    static packet_data get_udp_payload_for_loopback(packet_data& packet, ip_address& source, ip_address& destination);

    struct ethernet_header
    {
        /* Ethernet addresses are 6 bytes */
        static const int AddressLength = 6;
        uint8_t   mEtherDHost[AddressLength]; /* Destination host address */
        uint8_t   mEtherSHost[AddressLength]; /* Source host address */
        uint16_t  mEtherType; /* IP? ARP? RARP? etc */
    };

#if defined(TARGET_WIN)
    struct /*__attribute__((packed))*/ LinuxSllHeader
        #else
    struct __attribute__((packed))linux_sll_header
#endif
    {
        uint16_t mPacketType;
        uint16_t mARPHRD;
        uint16_t mAddressLength;
        uint64_t mAddress;
        uint16_t mProtocolType;
    };

    struct vlan_header
    {
        uint16_t mMagicId;
        uint16_t mData;
    };

    struct ip4_header
    {
        uint8_t   mVhl;                             /* version << 4 | header length >> 2 */
        uint8_t   mTos;                             /* type of service */
        uint16_t  mLen;                             /* total length */
        uint16_t  mId;                              /* identification */
        uint16_t  mOffset;                          /* fragment offset field */
#define IP_RF 0x8000                                /* reserved fragment flag */
#define IP_DF 0x4000                                /* dont fragment flag */
#define IP_MF 0x2000                                /* more fragments flag */
#define IP_OFFMASK 0x1fff                           /* mask for fragmenting bits */
        uint8_t   mTtl;                             /* time to live */
        uint8_t   mProtocol;		                /* protocol */
        uint16_t  mChecksum;                        /* checksum */
        in_addr   mSource,
                  mDestination; /* source and dest address */

        int headerLength() const
        {
            return (mVhl & 0x0f) * 4;
        }

        int version() const
        {
            return mVhl >> 4;
        }

        const in_addr& source4() const  { return mSource; }
        const in_addr& dest4() const    { return mDestination; }
        const in6_addr& source6() const { return (const in6_addr&)mSource; }
        const in6_addr& dest6() const   { return (const in6_addr&)mDestination; }
    };

    struct udp_header
    {
        uint16_t	mSourcePort;		        /* source port */
        uint16_t  mDestinationPort;
        uint16_t	mDatagramLength;		    /* datagram length */
        uint16_t  mDatagramChecksum;			/* datagram checksum */
    };


    struct tcp_header
    {
        uint16_t mSourcePort;	/* source port */
        uint16_t mDestinationPort;	/* destination port */
        uint32_t mSeqNo;		/* sequence number */
        uint32_t mAckNo;		/* acknowledgement number */
        uint32_t mDataOffset;	/* data offset, rsvd */
#define TH_OFF(th)	(((th)->th_offx2 & 0xf0) >> 4)
        uint8_t  mFlags;
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
        uint16_t mWindow;		/* window */
        uint16_t mChecksum;		/* checksum */
        uint16_t mUrgentPointer;		/* urgent pointer */
    };

};
#endif
