/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ip_address.h"
#include <string.h>
#include <stdlib.h>

ip_address::ip_address()
{
    memset(&this->addr, 0, sizeof addr);
}

void ip_address::set_ip(const in_addr& addr)
{
    set_family(AF_INET);
    this->addr.v4.sin_addr = addr;
}

void ip_address::set_ip(const in6_addr& addr)
{
    set_family(AF_INET6);
    this->addr.v6.sin6_addr = addr;
}

bool ip_address::set_address(const char* addr)
{
    const char* ps = strchr(addr, ':');
    if (ps != nullptr)
    {
        char* ip_text = (char*)alloca(ps - addr + 1);
        strncpy(ip_text, addr, ps - addr + 1);
        ip_text[ps - addr] = 0;
        int port = atoi(ps + 1);

        // Try IPv4
        set_family(AF_INET);
        int retcode = inet_pton(AF_INET, ip_text, &this->addr.v4.sin_addr);
        if (retcode != 1)
        {
            // Try IPv6
            set_family(AF_INET6);
            int retcode = inet_pton(AF_INET6, ip_text, &this->addr.v6.sin6_addr);
            if (retcode != 1)
                return false;
        }
        this->addr.v4.sin_port = htons(port);
    }
    else
    {
        // Try IPv4
        int retcode = inet_pton(AF_INET, ps, &this->addr.v4.sin_addr);
        if (retcode != 1)
        {
            // Try IPv6
            int retcode = inet_pton(AF_INET6, ps, &this->addr.v6.sin6_addr);
            if (retcode != 1)
                return false;
        }
    }
    return true;
}

void ip_address::set_port(uint16_t port)
{
    this->addr.v4.sin_port = htons(port);
}

uint16_t ip_address::port() const
{
    return ntohs(this->addr.v4.sin_port);
}

const in_addr& ip_address::ip4() const
{
    return this->addr.v4.sin_addr;
}

const in6_addr& ip_address::ip6() const
{
    return this->addr.v6.sin6_addr;
}

const sockaddr& ip_address::get_sockaddr() const
{
    return *reinterpret_cast<const struct sockaddr*>(&this->addr.v4);
}

size_t ip_address::get_sockaddrlen() const
{
    switch (this->addr.v4.sin_family)
    {
    case AF_INET:    return sizeof(addr.v4);
    case AF_INET6:   return sizeof(addr.v6);
    }
    return 0;
}

int ip_address::family() const
{
    return this->addr.v4.sin_family;
}

void ip_address::set_family(int family)
{
    this->addr.v4.sin_family = family;
}

