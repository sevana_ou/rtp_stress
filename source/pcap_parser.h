/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __PCAP_DATA_PARSER
#define __PCAP_DATA_PARSER

#include <time.h>
#include <functional>
#if defined(TARGET_WIN)
# include <WinSock2.h>
#endif


struct pcap_frame_header
{
    struct timeval ts;      /* time stamp */
    uint32_t caplen;        /* length of portion present */
    uint32_t len;           /* length this packet (off wire) */
};

class dump_parser
{
public:
    class impl;

    virtual ~dump_parser();

    typedef std::function<void(const pcap_frame_header* hdr, const void* data, int datalink)> callback;

    void set_callback(callback callback);
    callback get_callback() const;

    // Send incoming stream here
    virtual void parse(const void* buffer, size_t len) = 0;

    // Use it to create proper parser
    // signature is first 4 bytes from the stream
    static dump_parser* make(uint32_t signature);

protected:
    impl* impl;
    dump_parser();
};

class ostream;
class dump_builder
{
public:
    class impl;

    dump_builder(std::ostream& output);
    ~dump_builder();

    void set_datalink(int datalink);
    int get_datalink() const;

    void write(pcap_frame_header& header, const void* data);

protected:
    impl* impl;
};

#endif
