/* 
Copyright (c) 2020, SEVANA OÜ
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <string>
#include <memory.h>
#include <iomanip>
#include <map>
#include <regex>
#include <thread>
#include <fstream>
#include <atomic>

#include <stdint.h>
#include <unistd.h>

#if defined(TARGET_LINUX) || defined(TARGET_OSX)
# include <arpa/inet.h>
# include <sys/socket.h>
#endif
#include "CLI11.hpp"

#include "network_frame.h"
#include "rtp_utils.h"

#include "pcap_parser.h"

using namespace std;

const int DefaultTargetPort = 9000;
const int DefaultTestTime = 60000; // milliseconds

static std::string input_pcap,
                   target_addr;

static int         nr_of_threads = 1, nr_of_copies = 1, copies_delay_ms = 20,
                   interval_ms = 20, duration_ms = DefaultTestTime, timestamp_increment = 160;

bool parse_packet(int linktype, const pcap_frame_header* header, const u_char* data,
                 network_frame::packet_data& p)
{
    // Check if all packet is captured ok
    if (header->caplen != header->len)
        return false; // Truncated packet

    network_frame::packet_data packet(data, header->len);
    ip_address src, dst;
    switch (linktype)
    {
    case 228:   // LINKTYPE_IP4:
        p = network_frame::get_udp_payload_for_ip4(packet, src, dst);
        break;

    case 229:   // LINKTYPE_IP6
        p = network_frame::get_udp_payload_for_ip6(packet, src, dst);
        break;

    case 1:     // LINKTYPE_ETHERNET
        p = network_frame::get_udp_payload_for_ethernet(packet, src, dst);
        break;

    case 113:   // LINKTYPE_LINUX_SLL
        p = network_frame::get_udp_payload_for_sll(packet, src, dst);
        break;

    case 0:     // LINKTYPE_LOOPBACK
        p = network_frame::get_udp_payload_for_loopback(packet, src, dst);
        break;

    default:
        return false;
    }

    // Is there UDP payload?
    if (!p.data || !p.length)
        return false;

    // Is it RT(C)P ?
    if (!rtp_helper::is_rtp(p.data, p.length))
        return false;

    // Return success results
    return true;
}


int send_pcap()
{
    // Read file and send to library
    FILE* pcap_file = fopen(input_pcap.c_str(), "rb");
    if (!pcap_file)
    {
        std::cerr << "Failed to open .pcap file. Error: " << errno << std::endl;
        return EXIT_FAILURE;
    }

    // Read first 4 bytes
    uint32_t signature = 0;
    size_t was_read = 0;
    was_read = fread(&signature, 4, 1, pcap_file);
    if (was_read < 1)
    {
        std::cerr << "Failed to read signature from file." << std::endl;
        return EXIT_FAILURE;
    }
    std::unique_ptr<dump_parser> pcap_parser(dump_parser::make(signature));
    if (!pcap_parser)
    {
        std::cerr << "Failed to identify format." << std::endl;
        return EXIT_FAILURE;
    }

    struct packet_rec
    {
        packet_rec(const network_frame::packet_data& p)
            :data(p)
        {

        }
        network_frame::packet_data data;
        timeval tv;
    };

    std::vector<std::shared_ptr<packet_rec>> packet_list;
    uint32_t ssrc;
    bool ssrc_set = false;

    size_t max_packet_len = 0;

    // Lambda to handle packets
    auto handler = [&packet_list, &ssrc, &ssrc_set, &max_packet_len] (const pcap_frame_header* hdr, const void* data, int datalink)
    {
        network_frame::packet_data p;

        // Increate processed packet counter
        if (parse_packet(datalink, hdr, reinterpret_cast<const uint8_t*>(data), p))
        {
            auto pd = std::make_shared<packet_rec>(p);
            pd->tv = hdr->ts;
            if (ssrc_set)
            {
                uint32_t current_ssrc = rtp_helper::find_ssrc(pd->data.data, pd->data.length);
                if (ssrc != current_ssrc)
                {
                    std::cerr << "Multiple SSRC found in .pcap file: " << std::hex << current_ssrc
                              << ". Only single SSRC is allowed to use: " << std::hex << ssrc << std::endl;
                }
            }
            else
            {
                ssrc = rtp_helper::find_ssrc(pd->data.data, pd->data.length);
                ssrc_set = true;
            }

            if (pd->data.length > max_packet_len)
                max_packet_len = pd->data.length;

            packet_list.push_back(pd);
        }
    };
    pcap_parser->set_callback(handler);

    // Read .pcap file
    uint8_t buffer[1024];
    fseek(pcap_file, 0, SEEK_SET);
    while ((was_read = fread(buffer, 1, sizeof buffer, pcap_file)) > 0)
    {
        pcap_parser->parse(buffer, was_read);
    }

    fclose(pcap_file); pcap_file = nullptr;

    // Now - sending the streams

    // Number of finished threads
    std::atomic_int finished_threads;
    finished_threads = 0;

    // SSRC ID generator
    std::atomic_int ssrc_gen;
    ssrc_gen = 0;

    // Signal to finish for the threads
    std::atomic_bool shutdown;
    shutdown = false;

    // Convert address
    ip_address addr;
    if (!target_addr.empty())
    {
        if (!addr.set_address(target_addr.c_str()))
        {
            std::cerr << "Bad IP address: " << target_addr << std::endl;
            return EXIT_FAILURE;
        }
        if (addr.port() == 0)
            addr.set_port(DefaultTargetPort);
    }

    // Open output pcap file
    std::shared_ptr<std::ofstream> ofs;

    // Start sending N threads
    auto sender = [&packet_list, &finished_threads, &ssrc_gen, &shutdown, &addr]()
    {
        // Open socket
        auto s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        // Get current timepoint
        auto start_tp = std::chrono::steady_clock::now();
        auto finish_tp = start_tp + std::chrono::milliseconds(duration_ms);

        struct stream_ctx
        {
            bool active = false;
            uint32_t ssrc = 0;
            uint32_t packet_no = 0;
            std::chrono::steady_clock::time_point start_time;
        };

        // Prepare SSRC identifiers & schedule
        stream_ctx stream_list[nr_of_copies];

        for (size_t stream_idx = 0; stream_idx < static_cast<size_t>(nr_of_copies); stream_idx++)
        {
            auto& stream = stream_list[stream_idx];

            stream.ssrc = ++ssrc_gen;
            stream.start_time = start_tp + std::chrono::milliseconds(copies_delay_ms * stream_idx);
        }

        // Start sending
        for (auto current_tp = start_tp; current_tp < finish_tp && !shutdown; current_tp = std::chrono::steady_clock::now())
        {
            for (auto& stream: stream_list)
            {
                // Check if stream is active
                if (!stream.active && current_tp >= stream.start_time)
                    stream.active = true;

                if (!stream.active)
                    continue;

                // Rewrite SSRC & seqno
                auto& packet = packet_list[stream.packet_no % packet_list.size()];
                rtp_helper::set_ssrc(const_cast<uint8_t*>(packet->data.data), packet->data.length, stream.ssrc);
                rtp_helper::set_packet_no(const_cast<uint8_t*>(packet->data.data), packet->data.length, stream.packet_no % 0xFFFF);
                rtp_helper::set_timestamp(const_cast<uint8_t*>(packet->data.data), packet->data.length, stream.packet_no * timestamp_increment);
                stream.packet_no++;

                if (!target_addr.empty())
                {
                    int retcode = ::sendto(s, packet->data.data, packet->data.length, 0, &addr.get_sockaddr(), addr.get_sockaddrlen());
                    if (retcode == -1 && errno != ECONNREFUSED)
                        std::cerr << "sendto() failed with return code: " << retcode << ", errno: " << errno << std::endl;
                }
            }

            if (!target_addr.empty())
            {
                // Make packet interval
                std::this_thread::sleep_for(std::chrono::milliseconds(interval_ms));
            }
        }

        finished_threads++;

        close(s);
    };

    // Run sender threads
    std::vector<std::shared_ptr<std::thread>> workers;
    for (int i = 0; i < nr_of_threads; i++)
    {
        std::cout << "Start sending thread #" << i << std::endl;
        auto w = std::make_shared<std::thread>(sender);
        workers.push_back(w);
    }
    std::cout << "All threads are started. Sending..." << std::endl;

    // Wait until Ctrl-C or senders are not finished
    while (finished_threads < static_cast<int>(workers.size()))
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    std::cout << "Stopping worker threads..." << std::endl;

    for (auto& w: workers)
    {
        if (w)
        {
            if (w->joinable())
                w->join();
        }
    }
    std::cout << "Worker thread(s) stopped." << std::endl;
    return EXIT_SUCCESS;
}


static CLI::App init_options_descriptions()
{
    CLI::App app{".pcap RT(C)P sender"};
    app.add_option("--input",                         input_pcap,               "<path to .pcap dump>");
    app.add_option("--target",                        target_addr,              "target IP:port pair");
    app.add_option("--interval",                      interval_ms,              "time interval between packets (in milliseconds, default is 20ms)");
    app.add_option("--threads",                       nr_of_threads,            "number of sender threads (default is 1)");
    app.add_option("--copies",                        nr_of_copies,             "number of copies to send per thread");
    app.add_option("--copies-delay",                  copies_delay_ms,          "delay between copies start (in milliseconds, default is 20 ms)");
    app.add_option("--duration",                      duration_ms,              "duration of tests in milliseconds (default is 60000ms - 1 minute)");
    app.add_option("--timestamp-increment",           timestamp_increment,      "RTP timestamp increment (default is 160)");

    return app;
}

void show_help()
{
    auto opts = init_options_descriptions();
    std::cout << opts.help() << std::endl;
}

std::string parse_args(int argc, char* argv[])
{
    auto opts = init_options_descriptions();
    try
    {
        opts.parse(argc, argv);
    }
    catch(const CLI::CallForHelp&)
    {
        std::cout << opts.help() << std::endl;
        exit(EXIT_SUCCESS);
    }

    return std::string();
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "No input file specified." << std::endl;
        show_help();
        return EXIT_FAILURE;
    }

    parse_args(argc, argv);

    int exit_code = send_pcap();

    return exit_code;
}
